function addition() {
    let num1 = Number(document.getElementById('num1').value);
    let num2 = Number(document.getElementById('num2').value);

    if (isNaN(num1) || isNaN(num2)) {
      document.getElementById('result').innerHTML = "Invalid input!";
    }else if (num1 == "" || num2 == ""){
      document.getElementById('result').innerHTML = "Invalid input!";
    }else{
      document.getElementById('result').innerHTML = num1 + num2;
      document.getElementById('operation').innerHTML = "Addition";
    }

    return false;
   }

   function subtraction() {
      let num1 = Number(document.getElementById('num1').value);
      let num2 = Number(document.getElementById('num2').value);

      if (isNaN(num1) || isNaN(num2)) {
        document.getElementById('result').innerHTML = "Invalid input!";
      }else if (num1 == "" || num2 == ""){
        document.getElementById('result').innerHTML = "Invalid input!";
      }else{
        document.getElementById('result').innerHTML = num1 - num2;
        document.getElementById('operation').innerHTML = "Subtraction";
      }

      return false;
    }
    
    function multiplication() {
      let num1 = Number(document.getElementById('num1').value);
      let num2 = Number(document.getElementById('num2').value);

      if (isNaN(num1) || isNaN(num2)) {
        document.getElementById('result').innerHTML = "Invalid input!";
      }else if (num1 == "" || num2 == ""){
        document.getElementById('result').innerHTML = "Invalid input!";
      }else{
        document.getElementById('result').innerHTML = num1 * num2;
        document.getElementById('operation').innerHTML = "Multiplication";
      }

      return false;
    }

    function division() {
      let num1 = Number(document.getElementById('num1').value);
      let num2 = Number(document.getElementById('num2').value);

      if (isNaN(num1) || isNaN(num2)) {
        document.getElementById('result').innerHTML = "Invalid input!";
      }else if (num1 == "" || num2 == ""){
        document.getElementById('result').innerHTML = "Invalid input!";
      }else{
        document.getElementById('result').innerHTML = num1 / num2;
        document.getElementById('operation').innerHTML = "Division";
      }

      return false;
    }

    function modulus() {
      let num1 = Number(document.getElementById('num1').value);
      let num2 = Number(document.getElementById('num2').value);

      if (isNaN(num1) || isNaN(num2)) {
        document.getElementById('result').innerHTML = "Invalid input!";
      }else if (num1 == "" || num2 == ""){
        document.getElementById('result').innerHTML = "Invalid input!";
      }else{
        document.getElementById('result').innerHTML = num1 % num2;
        document.getElementById('operation').innerHTML = "Modulus";
      }

      return false;
    }

    function clear() {
      let num1 = Number(document.getElementById('num1').value = null);
      let num2 = Number(document.getElementById('num2').value = null);

      document.getElementById('result').innerHTML = "0";
      document.getElementById('operation').innerHTML = "Operation";
    }
    
    document.getElementById('add').addEventListener('click', addition);
    document.getElementById('subtract').addEventListener('click', subtraction);
    document.getElementById('multiply').addEventListener('click', multiplication);
    document.getElementById('divide').addEventListener('click', division);
    document.getElementById('mod').addEventListener('click', modulus);
    document.getElementById('clear').addEventListener('click', clear);